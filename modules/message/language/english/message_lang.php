<?php


$lang['message_id'] = 'Message Id';
$lang['message_text'] = 'Message Text';
$lang['added_date'] = 'Added Date';
$lang['modified_date'] = 'Modified Date';
$lang['status'] = 'Status';

$lang['create_message']='Create Message';
$lang['edit_message']='Edit Message';
$lang['delete_message']='Delete Message';
$lang['message_search']='Message Search';

$lang['message']='Message';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

