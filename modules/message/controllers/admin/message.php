<?php

class Message extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('message','message_model');
        $this->lang->module_load('message','message');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'message';
		$data['page'] = $this->config->item('template_admin') . "message/index";
		$data['module'] = 'message';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->message_model->count();
		paging('message_id');
		$this->_get_search_param();	
		$rows=$this->message_model->getMessages()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['message_text']!='')?$this->db->like('message_text',$params['search']['message_text']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		} 

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->message_model->getMessages()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->message_model->delete('MESSAGES',array('message_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('message_id'))
        {
            $success=$this->message_model->insert('MESSAGES',$data);
        }
        else
        {
            $success=$this->message_model->update('MESSAGES',$data,array('message_id'=>$data['message_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['message_id'] = $this->input->post('message_id');
$data['message_text'] = $this->input->post('message_text');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}