<?php

class Calendar extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('calendar','calendar_model');
        $this->lang->module_load('calendar','calendar');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'calendar';
		$data['page'] = $this->config->item('template_admin') . "calendar/index";
		$data['module'] = 'calendar';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$this->calendar_model->joins=array('HOLIDAYS');
		$total=$this->calendar_model->count();
		paging('calendar_id');
		$this->_get_search_param();	
		$rows=$this->calendar_model->getCalendars()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['holiday_id']!='')?$this->db->where('holiday_id',$params['search']['holiday_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->calendar_model->getCalendars()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->calendar_model->delete('CALENDARS',array('calendar_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('calendar_id'))
        {
            $success=$this->calendar_model->insert('CALENDARS',$data);
        }
        else
        {
        	$data['modified_date']=date('Y-m-d H:i:s');
            $success=$this->calendar_model->update('CALENDARS',$data,array('calendar_id'=>$data['calendar_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['calendar_id'] = $this->input->post('calendar_id');
$data['holiday_id'] = $this->input->post('holiday_id');
$data['holiday_date'] = $this->input->post('holiday_date');

        return $data;
   }
   
   	
	    
}