<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('calendar_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="calendar-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('holiday_id')?></label>:</td>
<td><input type="text" name="search[holiday_id]" id="search_holiday_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('holiday_date')?></label>:</td>
<td><input type="text" name="date[holiday_date][from]" id="search_holiday_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[holiday_date][to]" id="search_holiday_date_to"  class="easyui-datebox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="calendar-table" data-options="pagination:true,title:'<?php  echo lang('calendar')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'calendar_id',sortable:true" width="30"><?php echo lang('calendar_id')?></th>
<th data-options="field:'holiday_title',sortable:true" width="50"><?php echo lang('holiday_id')?></th>
<th data-options="field:'holiday_date',sortable:true" width="50"><?php echo lang('holiday_date')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_calendar')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_calendar')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit calendar form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-calendar" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('holiday_id')?>:</label></td>
					  <td width="66%"><input name="holiday_id" id="holiday_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('holiday_date')?>:</label></td>
					  <td width="66%"><input name="holiday_date" id="holiday_date" class="easyui-datebox" required="true"></td>
		       </tr><input type="hidden" name="calendar_id" id="calendar_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		<?php easyui_combobox('holiday_id','HOLIDAY')?>
		$('#clear').click(function(){
			$('#calendar-search-form').form('clear');
			$('#calendar-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#calendar-table').datagrid({
				queryParams:{data:$('#calendar-search-form').serialize()}
				});
		});		
		$('#calendar-table').datagrid({
			url:'<?php  echo site_url('calendar/admin/calendar/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_calendar')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removecalendar('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_calendar')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-calendar').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_calendar')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#calendar-table').datagrid('getRows')[index];
		if (row){
			$('#form-calendar').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_calendar')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removecalendar(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#calendar-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('calendar/admin/calendar/delete_json')?>', {id:[row.calendar_id]}, function(){
					$('#calendar-table').datagrid('deleteRow', index);
					$('#calendar-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#calendar-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].calendar_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('calendar/admin/calendar/delete_json')?>',{id:selected},function(data){
						$('#calendar-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-calendar').form('submit',{
			url: '<?php  echo site_url('calendar/admin/calendar/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-calendar').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#calendar-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>