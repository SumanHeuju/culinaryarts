<?php


$lang['employee_id'] = 'Employee Id';
$lang['employee_category_id'] = 'Employee Category Id';
$lang['employee_type_id'] = 'Employee Type Id';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['phone_number'] = 'Phone Number';
$lang['address'] = 'Address';
$lang['added_date'] = 'Added Date';
$lang['modified_date'] = 'Modified Date';
$lang['status'] = 'Status';

$lang['create_employee']='Create Employee';
$lang['edit_employee']='Edit Employee';
$lang['delete_employee']='Delete Employee';
$lang['employee_search']='Employee Search';

$lang['employee']='Employee';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

