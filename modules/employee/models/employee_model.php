<?php
class Employee_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('EMPLOYEES'=>$this->prefix.'employees',
                             'EMPLOYEE_CATEGORIES'=>$this->prefix.'employee_categories',
                             'EMPLOYEE_TYPES'=>$this->prefix.'employee_types');
        
		$this->_JOINS=array('EMPLOYEE_CATEGORIES'=>array('join_type'=>'LEFT','join_field'=>'employee_categories.employee_category_id=employees.employee_category_id',
                                           'select'=>'employee_categories.category','alias'=>'employee_categories'),
        'EMPLOYEE_TYPES'=>array('join_type'=>'LEFT','join_field'=>'employee_types.employee_type_id=employees.employee_type_id',
                                           'select'=>'employee_types.type','alias'=>'employee_types'),
                           
                            );        
    }
    
    public function getEmployees($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='employees.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['EMPLOYEES']. ' employees');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['EMPLOYEES'].' employees');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}