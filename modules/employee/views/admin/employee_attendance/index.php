<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('employee_attendance_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="employee_attendance-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('employee_id')?></label>:</td>
<td><input type="text" name="search[employee_id]" id="search_employee_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('date')?></label>:</td>
<td><input type="text" name="date[date][from]" id="search_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date][to]" id="search_date_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('check_in')?></label>:</td>
<td><input type="text" name="search[check_in]" id="search_check_in"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('check_out')?></label>:</td>
<td><input type="text" name="search[check_out]" id="search_check_out"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('is_sync')?></label>:</td>
<td><input type="radio" name="search[is_sync]" id="search_is_sync1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[is_sync]" id="search_is_sync0" value="0"/><?php echo lang('general_no')?></td>
<td><label><?php echo lang('is_message_sent')?></label>:</td>
<td><input type="radio" name="search[is_message_sent]" id="search_is_message_sent1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[is_message_sent]" id="search_is_message_sent0" value="0"/><?php echo lang('general_no')?></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="employee_attendance-table" data-options="pagination:true,title:'<?php  echo lang('employee_attendance')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'employee_attendance_id',sortable:true" width="30"><?php echo lang('employee_attendance_id')?></th>
<th data-options="field:'name',sortable:true" width="50"><?php echo lang('employee_id')?></th>
<th data-options="field:'date',sortable:true" width="50"><?php echo lang('date')?></th>
<th data-options="field:'check_in',sortable:true" width="50"><?php echo lang('check_in')?></th>
<th data-options="field:'check_out',sortable:true" width="50"><?php echo lang('check_out')?></th>
<th data-options="field:'is_sync',sortable:true" width="50"><?php echo lang('is_sync')?></th>
<th data-options="field:'is_message_sent',sortable:true" width="50"><?php echo lang('is_message_sent')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_employee_attendance')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_employee_attendance')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit employee_attendance form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-employee_attendance" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('employee_id')?>:</label></td>
					  <td width="66%"><input name="employee_id" id="employee_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date')?>:</label></td>
					  <td width="66%"><input name="date" id="date" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('check_in')?>:</label></td>
					  <td width="66%"><input name="check_in" id="check_in" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('check_out')?>:</label></td>
					  <td width="66%"><input name="check_out" id="check_out" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('is_sync')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="is_sync" id="is_sync1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="is_sync" id="is_sync0" /><?php echo lang("general_no")?></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('is_message_sent')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="is_message_sent" id="is_message_sent1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="is_message_sent" id="is_message_sent0" /><?php echo lang("general_no")?></td>
		       </tr><input type="hidden" name="employee_attendance_id" id="employee_attendance_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		<?php easyui_combobox('employee_id','EMPLOYEE')?>
		$('#clear').click(function(){
			$('#employee_attendance-search-form').form('clear');
			$('#employee_attendance-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#employee_attendance-table').datagrid({
				queryParams:{data:$('#employee_attendance-search-form').serialize()}
				});
		});		
		$('#employee_attendance-table').datagrid({
			url:'<?php  echo site_url('employee/admin/attendance/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_employee_attendance')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeemployee_attendance('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_employee_attendance')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-employee_attendance').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_employee_attendance')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#employee_attendance-table').datagrid('getRows')[index];
		if (row){
			$('#form-employee_attendance').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_employee_attendance')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeemployee_attendance(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#employee_attendance-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('employee/admin/attendance/delete_json')?>', {id:[row.employee_attendance_id]}, function(){
					$('#employee_attendance-table').datagrid('deleteRow', index);
					$('#employee_attendance-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#employee_attendance-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].employee_attendance_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('employee/admin/attendance/delete_json')?>',{id:selected},function(data){
						$('#employee_attendance-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-employee_attendance').form('submit',{
			url: '<?php  echo site_url('employee/admin/attendance/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-employee_attendance').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#employee_attendance-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>