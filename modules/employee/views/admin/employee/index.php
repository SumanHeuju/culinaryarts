<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('employee_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="employee-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('employee_category_id')?></label>:</td>
<td><input type="text" name="search[employee_category_id]" id="search_employee_category_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('employee_type_id')?></label>:</td>
<td><input type="text" name="search[employee_type_id]" id="search_employee_type_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('name')?></label>:</td>
<td><input type="text" name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('email')?></label>:</td>
<td><input type="text" name="search[email]" id="search_email"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('phone_number')?></label>:</td>
<td><input type="text" name="search[phone_number]" id="search_phone_number"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('address')?></label>:</td>
<td><input type="text" name="search[address]" id="search_address"  class="easyui-validatebox"/></td>
</tr>
<!-- <tr>
<td><label><?php echo lang('added_date')?></label>:</td>
<td><input type="text" name="search[added_date]" id="search_added_date"  class="easyui-datetimebox"/></td>
<td><label><?php echo lang('modified_date')?></label>:</td>
<td><input type="text" name="search[modified_date]" id="search_modified_date"  class="easyui-datetimebox"/></td>
</tr> -->
<tr>
<td><label><?php echo lang('status')?></label>:</td>
<td><input type="radio" name="search[status]" id="search_status1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[status]" id="search_status0" value="0"/><?php echo lang('general_no')?></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="employee-table" data-options="pagination:true,title:'<?php  echo lang('employee')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'employee_id',sortable:true" width="30"><?php echo lang('employee_id')?></th>
<th data-options="field:'name',sortable:true" width="50"><?php echo lang('name')?></th>
    
<th data-options="field:'category',sortable:true" width="50"><?php echo lang('employee_category_id')?></th>
<th data-options="field:'type',sortable:true" width="50"><?php echo lang('employee_type_id')?></th>
<th data-options="field:'email',sortable:true" width="50"><?php echo lang('email')?></th>
<th data-options="field:'phone_number',sortable:true" width="50"><?php echo lang('phone_number')?></th>
<th data-options="field:'address',sortable:true" width="50"><?php echo lang('address')?></th>
<!-- <th data-options="field:'added_date',sortable:true" width="50"><?php echo lang('added_date')?></th>
<th data-options="field:'modified_date',sortable:true" width="50"><?php echo lang('modified_date')?></th> -->
<th data-options="field:'status',sortable:true,formatter:formatStatus" width="30" align="center"><?php echo lang('status')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_employee')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_employee')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit employee form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-employee" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('employee_category_id')?>:</label></td>
					  <td width="66%"><input name="employee_category_id" id="employee_category_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('employee_type_id')?>:</label></td>
					  <td width="66%"><input name="employee_type_id" id="employee_type_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('name')?>:</label></td>
					  <td width="66%"><input name="name" id="name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('email')?>:</label></td>
					  <td width="66%"><input name="email" id="email" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('phone_number')?>:</label></td>
					  <td width="66%"><input name="phone_number" id="phone_number" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('address')?>:</label></td>
					  <td width="66%"><input name="address" id="address" class="easyui-validatebox" required="true"></td>
		       </tr><!-- <tr>
		              <td width="34%" ><label><?php echo lang('added_date')?>:</label></td>
					  <td width="66%"><input name="added_date" id="added_date" class="easyui-datetimebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('modified_date')?>:</label></td>
					  <td width="66%"><input name="modified_date" id="modified_date" class="easyui-datetimebox" required="true"></td>
		       </tr> --><tr>
		              <td width="34%" ><label><?php echo lang('status')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="status" id="status1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="status" id="status0" /><?php echo lang("general_no")?></td>
		       </tr><input type="hidden" name="employee_id" id="employee_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		<?php easyui_combobox('employee_category_id','EMPLOYEE_CATEGORY')?>
		<?php easyui_combobox('employee_type_id','EMPLOYEE_TYPE')?>
		$('#clear').click(function(){
			$('#employee-search-form').form('clear');
			$('#employee-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#employee-table').datagrid({
				queryParams:{data:$('#employee-search-form').serialize()}
				});
		});		
		$('#employee-table').datagrid({
			url:'<?php  echo site_url('employee/admin/employee/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_employee')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeemployee('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_employee')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-employee').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_employee')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#employee-table').datagrid('getRows')[index];
		if (row){
			$('#form-employee').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_employee')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeemployee(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#employee-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('employee/admin/employee/delete_json')?>', {id:[row.employee_id]}, function(){
					$('#employee-table').datagrid('deleteRow', index);
					$('#employee-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#employee-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].employee_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('employee/admin/employee/delete_json')?>',{id:selected},function(data){
						$('#employee-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-employee').form('submit',{
			url: '<?php  echo site_url('employee/admin/employee/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-employee').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#employee-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>