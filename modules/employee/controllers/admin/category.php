<?php

class Category extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('employee','employee_category_model');
        $this->lang->module_load('employee','employee_category');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'employee_category';
		$data['page'] = $this->config->item('template_admin') . "category/index";
		$data['module'] = 'employee';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->employee_category_model->count();
		paging('employee_category_id');
		$this->_get_search_param();	
		$rows=$this->employee_category_model->getEmployeeCategories()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['category']!='')?$this->db->like('category',$params['search']['category']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->employee_category_model->getEmployeeCategories()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->employee_category_model->delete('EMPLOYEE_CATEGORIES',array('employee_category_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('employee_category_id'))
        {
            $success=$this->employee_category_model->insert('EMPLOYEE_CATEGORIES',$data);
        }
        else
        {
            $success=$this->employee_category_model->update('EMPLOYEE_CATEGORIES',$data,array('employee_category_id'=>$data['employee_category_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['employee_category_id'] = $this->input->post('employee_category_id');
$data['category'] = $this->input->post('category');

        return $data;
   }
   
   	
	    
}