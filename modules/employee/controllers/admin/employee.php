<?php

class Employee extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('employee','employee_model');
        $this->lang->module_load('employee','employee');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'employee';
		$data['page'] = $this->config->item('template_admin') . "employee/index";
		$data['module'] = 'employee';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$this->employee_model->joins=array('EMPLOYEE_CATEGORIES','EMPLOYEE_TYPES');
		$total=$this->employee_model->count();
		paging('employee_id');
		$this->_get_search_param();	
		$rows=$this->employee_model->getEmployees()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['employee_category_id']!='')?$this->db->where('employee_category_id',$params['search']['employee_category_id']):'';
($params['search']['employee_type_id']!='')?$this->db->where('employee_type_id',$params['search']['employee_type_id']):'';
($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';
($params['search']['phone_number']!='')?$this->db->like('phone_number',$params['search']['phone_number']):'';
($params['search']['address']!='')?$this->db->like('address',$params['search']['address']):'';
($params['search']['added_date']!='')?$this->db->like('added_date',$params['search']['added_date']):'';
($params['search']['modified_date']!='')?$this->db->like('modified_date',$params['search']['modified_date']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->employee_model->getEmployees()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->employee_model->delete('EMPLOYEES',array('employee_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('employee_id'))
        {
            $success=$this->employee_model->insert('EMPLOYEES',$data);
        }
        else
        {
        	$data['modified_date']=date('Y-m-d H:i:s');
            $success=$this->employee_model->update('EMPLOYEES',$data,array('employee_id'=>$data['employee_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['employee_id'] = $this->input->post('employee_id');
$data['employee_category_id'] = $this->input->post('employee_category_id');
$data['employee_type_id'] = $this->input->post('employee_type_id');
$data['name'] = $this->input->post('name');
$data['email'] = $this->input->post('email');
$data['phone_number'] = $this->input->post('phone_number');
$data['address'] = $this->input->post('address');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}