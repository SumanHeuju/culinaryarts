<?php

class Attendance extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('employee','employee_attendance_model');
        $this->lang->module_load('employee','employee_attendance');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'employee_attendance';
		$data['page'] = $this->config->item('template_admin') . "employee_attendance/index";
		$data['module'] = 'employee';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();
		$this->employee_attendance_model->joins=array('EMPLOYEES');	
		$total=$this->employee_attendance_model->count();
		paging('employee_attendance_id');
		$this->_get_search_param();	
		$rows=$this->employee_attendance_model->getEmployeeAttendances()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['employee_id']!='')?$this->db->where('employee_id',$params['search']['employee_id']):'';
($params['search']['check_in']!='')?$this->db->like('check_in',$params['search']['check_in']):'';
($params['search']['check_out']!='')?$this->db->like('check_out',$params['search']['check_out']):'';
(isset($params['search']['is_sync']))?$this->db->where('is_sync',$params['search']['is_sync']):'';
(isset($params['search']['is_message_sent']))?$this->db->where('is_message_sent',$params['search']['is_message_sent']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->employee_attendance_model->getEmployeeAttendances()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->employee_attendance_model->delete('EMPLOYEE_ATTENDANCES',array('employee_attendance_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('employee_attendance_id'))
        {
            $success=$this->employee_attendance_model->insert('EMPLOYEE_ATTENDANCES',$data);
        }
        else
        {
            $success=$this->employee_attendance_model->update('EMPLOYEE_ATTENDANCES',$data,array('employee_attendance_id'=>$data['employee_attendance_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['employee_attendance_id'] = $this->input->post('employee_attendance_id');
$data['employee_id'] = $this->input->post('employee_id');
$data['date'] = $this->input->post('date');
$data['check_in'] = $this->input->post('check_in');
$data['check_out'] = $this->input->post('check_out');
$data['is_sync'] = $this->input->post('is_sync');
$data['is_message_sent'] = $this->input->post('is_message_sent');

        return $data;
   }
   
   	
	    
}