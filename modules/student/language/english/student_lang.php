<?php


$lang['student_id'] = 'Student Id';
$lang['first_name'] = 'First Name';
$lang['last_name'] = 'Last Name';
$lang['date_of_birth'] = 'Date Of Birth';
$lang['gender'] = 'Gender';
$lang['email'] = 'Email';
$lang['contact_no'] = 'Contact No';
$lang['guardian_name'] = 'Guardian Name';
$lang['guardian_contact_no'] = 'Guardian Contact No';
$lang['added_date'] = 'Added Date';
$lang['modified_date'] = 'Modified Date';
$lang['status'] = 'Status';

$lang['create_student']='Create Student';
$lang['edit_student']='Edit Student';
$lang['delete_student']='Delete Student';
$lang['student_search']='Student Search';

$lang['student']='Student';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

