<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('student_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="student-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('first_name')?></label>:</td>
<td><input type="text" name="search[first_name]" id="search_first_name"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('last_name')?></label>:</td>
<td><input type="text" name="search[last_name]" id="search_last_name"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('date_of_birth')?></label>:</td>
<td><input type="text" name="date[date_of_birth][from]" id="search_date_of_birth_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_of_birth][to]" id="search_date_of_birth_to"  class="easyui-datebox"/></td>
<td><label><?php echo lang('gender')?></label>:</td>
<td><input type="text" name="search[gender]" id="search_gender"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('email')?></label>:</td>
<td><input type="text" name="search[email]" id="search_email"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('contact_no')?></label>:</td>
<td><input type="text" name="search[contact_no]" id="search_contact_no"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('guardian_name')?></label>:</td>
<td><input type="text" name="search[guardian_name]" id="search_guardian_name"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('guardian_contact_no')?></label>:</td>
<td><input type="text" name="search[guardian_contact_no]" id="search_guardian_contact_no"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('status')?></label>:</td>
<td><input type="radio" name="search[status]" id="search_status1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[status]" id="search_status0" value="0"/><?php echo lang('general_no')?></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="student-table" data-options="pagination:true,title:'<?php  echo lang('student')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'student_id',sortable:true" width="30"><?php echo lang('student_id')?></th>
<th data-options="field:'first_name',sortable:true" width="50"><?php echo lang('first_name')?></th>
<th data-options="field:'last_name',sortable:true" width="50"><?php echo lang('last_name')?></th>
<th data-options="field:'date_of_birth',sortable:true" width="50"><?php echo lang('date_of_birth')?></th>
<th data-options="field:'gender',sortable:true" width="50"><?php echo lang('gender')?></th>
<th data-options="field:'email',sortable:true" width="50"><?php echo lang('email')?></th>
<th data-options="field:'contact_no',sortable:true" width="50"><?php echo lang('contact_no')?></th>
<th data-options="field:'guardian_name',sortable:true" width="50"><?php echo lang('guardian_name')?></th>
<th data-options="field:'guardian_contact_no',sortable:true" width="50"><?php echo lang('guardian_contact_no')?></th>
<!--<th data-options="field:'added_date',sortable:true" width="50"><?php echo lang('added_date')?></th>
<th data-options="field:'modified_date',sortable:true" width="50"><?php echo lang('modified_date')?></th>-->
<th data-options="field:'status',sortable:true,formatter:formatStatus" width="30" align="center"><?php echo lang('status')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_student')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_student')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit student form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-student" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('first_name')?>:</label></td>
					  <td width="66%"><input name="first_name" id="first_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('last_name')?>:</label></td>
					  <td width="66%"><input name="last_name" id="last_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_of_birth')?>:</label></td>
					  <td width="66%"><input name="date_of_birth" id="date_of_birth" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('gender')?>:</label></td>
					  <td width="66%"><input name="gender" id="gender" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('email')?>:</label></td>
					  <td width="66%"><input name="email" id="email" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('contact_no')?>:</label></td>
					  <td width="66%"><input name="contact_no" id="contact_no" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('guardian_name')?>:</label></td>
					  <td width="66%"><input name="guardian_name" id="guardian_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('guardian_contact_no')?>:</label></td>
					  <td width="66%"><input name="guardian_contact_no" id="guardian_contact_no" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('status')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="status" id="status1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="status" id="status0" /><?php echo lang("general_no")?></td>
		       </tr><input type="hidden" name="student_id" id="student_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#student-search-form').form('clear');
			$('#student-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#student-table').datagrid({
				queryParams:{data:$('#student-search-form').serialize()}
				});
		});		
		$('#student-table').datagrid({
			url:'<?php  echo site_url('student/admin/student/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_student')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removestudent('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_student')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-student').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_student')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#student-table').datagrid('getRows')[index];
		if (row){
			$('#form-student').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_student')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removestudent(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#student-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('student/admin/student/delete_json')?>', {id:[row.student_id]}, function(){
					$('#student-table').datagrid('deleteRow', index);
					$('#student-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#student-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].student_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('student/admin/student/delete_json')?>',{id:selected},function(data){
						$('#student-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-student').form('submit',{
			url: '<?php  echo site_url('student/admin/student/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-student').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#student-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>