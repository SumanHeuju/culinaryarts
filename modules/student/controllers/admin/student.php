<?php

class Student extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('student','student_model');
        $this->lang->module_load('student','student');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'student';
		$data['page'] = $this->config->item('template_admin') . "student/index";
		$data['module'] = 'student';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->student_model->count();
		paging('student_id');
		$this->_get_search_param();	
		$rows=$this->student_model->getStudents()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['first_name']!='')?$this->db->like('first_name',$params['search']['first_name']):'';
($params['search']['last_name']!='')?$this->db->like('last_name',$params['search']['last_name']):'';
($params['search']['gender']!='')?$this->db->like('gender',$params['search']['gender']):'';
($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';
($params['search']['contact_no']!='')?$this->db->like('contact_no',$params['search']['contact_no']):'';
($params['search']['guardian_name']!='')?$this->db->like('guardian_name',$params['search']['guardian_name']):'';
($params['search']['guardian_contact_no']!='')?$this->db->like('guardian_contact_no',$params['search']['guardian_contact_no']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->student_model->getStudents()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->student_model->delete('STUDENTS',array('student_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('student_id'))
        {
            $success=$this->student_model->insert('STUDENTS',$data);
        }
        else
        {
        	$data['modified_date']=date('Y-m-d H:i:s');
            $success=$this->student_model->update('STUDENTS',$data,array('student_id'=>$data['student_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['student_id'] = $this->input->post('student_id');
$data['first_name'] = $this->input->post('first_name');
$data['last_name'] = $this->input->post('last_name');
$data['date_of_birth'] = $this->input->post('date_of_birth');
$data['gender'] = $this->input->post('gender');
$data['email'] = $this->input->post('email');
$data['contact_no'] = $this->input->post('contact_no');
$data['guardian_name'] = $this->input->post('guardian_name');
$data['guardian_contact_no'] = $this->input->post('guardian_contact_no');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}