<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('sms_log_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="sms_log-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('student_attendance_id')?></label>:</td>
<td><input type="text" name="search[student_attendance_id]" id="search_student_attendance_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('sent_date')?></label>:</td>
<td><input type="text" name="search[sent_date]" id="search_sent_date"  class="easyui-datetimebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('confirm_date')?></label>:</td>
<td><input type="text" name="search[confirm_date]" id="search_confirm_date"  class="easyui-datetimebox"/></td>
<td><label><?php echo lang('message_id')?></label>:</td>
<td><input type="text" name="search[message_id]" id="search_message_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('status')?></label>:</td>
<td><input type="radio" name="search[status]" id="search_status1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[status]" id="search_status0" value="0"/><?php echo lang('general_no')?></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="sms_log-table" data-options="pagination:true,title:'<?php  echo lang('sms_log')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'sms_log_id',sortable:true" width="30"><?php echo lang('sms_log_id')?></th>
<th data-options="field:'date',sortable:true" width="50"><?php echo lang('student_attendance_id')?></th>
<th data-options="field:'first_name',sortable:true" width="50"><?php echo lang('student_name')?></th>
<!-- <th data-options="field:'sent_date',sortable:true" width="50"><?php echo lang('sent_date')?></th>
<th data-options="field:'confirm_date',sortable:true" width="50"><?php echo lang('confirm_date')?></th> -->
<th data-options="field:'message_text',sortable:true" width="50"><?php echo lang('message_id')?></th>
<th data-options="field:'status',sortable:true,formatter:formatStatus" width="30" align="center"><?php echo lang('status')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <!-- <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_sms_log')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_sms_log')?>"><?php  echo lang('remove_selected')?></a>
    </p> -->

</div> 

<!--for create and edit sms_log form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-sms_log" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('student_attendance_id')?>:</label></td>
					  <td width="66%"><input name="student_attendance_id" id="student_attendance_id" class="easyui-combobox" required="true"></td>  
		       </tr><tr>
		       <td width="34%" ><label><?php echo lang('student_attendance_id')?>:</label></td>
					  <td width="66%"><input name="student_name" id="student_name" class="easyui-combobox" required="true"></td>
		       </tr><!-- <tr>
		              <td width="34%" ><label><?php echo lang('sent_date')?>:</label></td>
					  <td width="66%"><input name="sent_date" id="sent_date" class="easyui-datetimebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('confirm_date')?>:</label></td>
					  <td width="66%"><input name="confirm_date" id="confirm_date" class="easyui-datetimebox" required="true"></td>
		       </tr> --><tr>
		              <td width="34%" ><label><?php echo lang('message_id')?>:</label></td>
					  <td width="66%"><input name="message_id" id="message_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('status')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="status" id="status1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="status" id="status0" /><?php echo lang("general_no")?></td>
		       </tr><input type="hidden" name="sms_log_id" id="sms_log_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		<?php easyui_combobox('message_id','MESSAGE')?>
		<?php easyui_combobox('student_attendance_id','STUDENT_ATTENDANCE')?>
		<?php easyui_combobox('student_name','STUDENT')?>
		$('#clear').click(function(){
			$('#sms_log-search-form').form('clear');
			$('#sms_log-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#sms_log-table').datagrid({
				queryParams:{data:$('#sms_log-search-form').serialize()}
				});
		});		
		$('#sms_log-table').datagrid({
			url:'<?php  echo site_url('sms_log/admin/sms_log/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		// var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_sms_log')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="details()" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('details')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-search"></span></span></a>';
		return d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	// function create(){
	// 	//Create code here
	// 	$('#form-sms_log').form('clear');
	// 	$('#dlg').window('open').window('setTitle','<?php  echo lang('create_sms_log')?>');
	// 	//uploadReady(); //Uncomment This function if ajax uploading
	// }	

	// function edit(index)
	// {
	// 	var row = $('#sms_log-table').datagrid('getRows')[index];
	// 	if (row){
	// 		$('#form-sms_log').form('load',row);
	// 		//uploadReady(); //Uncomment This function if ajax uploading
	// 		$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_sms_log')?>');
	// 	}
	// 	else
	// 	{
	// 		$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
	// 	}		
	// }
	
		
	// function removesms_log(index)
	// {
	// 	$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
	// 		if (r){
	// 			var row = $('#sms_log-table').datagrid('getRows')[index];
	// 			$.post('<?php  echo site_url('sms_log/admin/sms_log/delete_json')?>', {id:[row.sms_log_id]}, function(){
	// 				$('#sms_log-table').datagrid('deleteRow', index);
	// 				$('#sms_log-table').datagrid('reload');
	// 			});

	// 		}
	// 	});
	// }
	
	// function removeSelected()
	// {
	// 	var rows=$('#sms_log-table').datagrid('getSelections');
	// 	if(rows.length>0)
	// 	{
	// 		selected=[];
	// 		for(i=0;i<rows.length;i++)
	// 		{
	// 			selected.push(rows[i].sms_log_id);
	// 		}
			
	// 		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
	// 			if(r){				
	// 				$.post('<?php  echo site_url('sms_log/admin/sms_log/delete_json')?>',{id:selected},function(data){
	// 					$('#sms_log-table').datagrid('reload');
	// 				});
	// 			}
				
	// 		});
			
	// 	}
	// 	else
	// 	{
	// 		$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
	// 	}
		
	// }
	
	// function save()
	// {
	// 	$('#form-sms_log').form('submit',{
	// 		url: '<?php  echo site_url('sms_log/admin/sms_log/save')?>',
	// 		onSubmit: function(){
	// 			return $(this).form('validate');
	// 		},
	// 		success: function(result){
	// 			var result = eval('('+result+')');
	// 			if (result.success)
	// 			{
	// 				$('#form-sms_log').form('clear');
	// 				$('#dlg').window('close');		// close the dialog
	// 				$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
	// 				$('#sms_log-table').datagrid('reload');	// reload the user data
	// 			} 
	// 			else 
	// 			{
	// 				$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
	// 			} //if close
	// 		}//success close
		
	// 	});		
		
	// }
	
	
</script>