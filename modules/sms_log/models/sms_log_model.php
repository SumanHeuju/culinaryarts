<?php
class Sms_log_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('SMS_LOGS'=>$this->prefix.'sms_logs',
                             'MESSAGES'=>$this->prefix.'messages',
                             'STUDENT_ATTENDANCES'=>$this->prefix.'student_attendances',
                             'STUDENTS'=>$this->prefix.'students');
		$this->_JOINS=array('MESSAGES'=>array('join_type'=>'LEFT','join_field'=>'messages.message_id=sms_logs.message_id',
                                           'select'=>'messages.message_text','alias'=>'messages'),

                            'STUDENT_ATTENDANCES'=>array('join_type'=>'LEFT','join_field'=>'student_attendances.student_attendance_id=sms_logs.student_attendance_id',
                                           'select'=>'student_attendances.date','alias'=>'student_attendances'),
                            'STUDENTS'=>array('join_type'=>'LEFT','join_field'=>'students.student_id=student_attendances.student_id',
                                           'select'=>'students.first_name','alias'=>'students'),
                           
                            );        
    }
    
    public function getSmsLogs($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='sms_logs.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['SMS_LOGS']. ' sms_logs');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['SMS_LOGS'].' sms_logs');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}