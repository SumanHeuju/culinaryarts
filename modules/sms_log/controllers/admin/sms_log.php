<?php

class Sms_log extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('sms_log','sms_log_model');
        $this->lang->module_load('sms_log','sms_log');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'sms_log';
		$data['page'] = $this->config->item('template_admin') . "sms_log/index";
		$data['module'] = 'sms_log';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$this->sms_log_model->joins=array('MESSAGES','STUDENT_ATTENDANCES','STUDENTS');
		$total=$this->sms_log_model->count();
		paging('sms_log_id');
		$this->_get_search_param();	
		$rows=$this->sms_log_model->getSmsLogs()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['student_attendance_id']!='')?$this->db->where('student_attendance_id',$params['search']['student_attendance_id']):'';
($params['search']['sent_date']!='')?$this->db->like('sent_date',$params['search']['sent_date']):'';
($params['search']['confirm_date']!='')?$this->db->like('confirm_date',$params['search']['confirm_date']):'';
($params['search']['message_id']!='')?$this->db->where('message_id',$params['search']['message_id']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->sms_log_model->getSmsLogs()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->sms_log_model->delete('SMS_LOGS',array('sms_log_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('sms_log_id'))
        {
            $success=$this->sms_log_model->insert('SMS_LOGS',$data);
        }
        else
        {
            $success=$this->sms_log_model->update('SMS_LOGS',$data,array('sms_log_id'=>$data['sms_log_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['sms_log_id'] = $this->input->post('sms_log_id');
$data['student_attendance_id'] = $this->input->post('student_attendance_id');
$data['sent_date'] = $this->input->post('sent_date');
$data['confirm_date'] = $this->input->post('confirm_date');
$data['message_id'] = $this->input->post('message_id');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}