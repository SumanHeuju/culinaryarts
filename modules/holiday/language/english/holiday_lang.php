<?php


$lang['holiday_id'] = 'Holiday Id';
$lang['holiday_title'] = 'Holiday Title';
$lang['added_date'] = 'Added Date';
$lang['modified_date'] = 'Modified Date';
$lang['status'] = 'Status';

$lang['create_holiday']='Create Holiday';
$lang['edit_holiday']='Edit Holiday';
$lang['delete_holiday']='Delete Holiday';
$lang['holiday_search']='Holiday Search';

$lang['holiday']='Holiday';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

