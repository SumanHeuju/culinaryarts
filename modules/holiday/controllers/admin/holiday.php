<?php

class Holiday extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('holiday','holiday_model');
        $this->lang->module_load('holiday','holiday');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'holiday';
		$data['page'] = $this->config->item('template_admin') . "holiday/index";
		$data['module'] = 'holiday';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->holiday_model->count();
		paging('holiday_id');
		$this->_get_search_param();	
		$rows=$this->holiday_model->getHolidays()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['holiday_title']!='')?$this->db->like('holiday_title',$params['search']['holiday_title']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->holiday_model->getHolidays()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->holiday_model->delete('HOLIDAYS',array('holiday_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('holiday_id'))
        {
            $success=$this->holiday_model->insert('HOLIDAYS',$data);
        }
        else
        {
            $success=$this->holiday_model->update('HOLIDAYS',$data,array('holiday_id'=>$data['holiday_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['holiday_id'] = $this->input->post('holiday_id');
$data['holiday_title'] = $this->input->post('holiday_title');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}