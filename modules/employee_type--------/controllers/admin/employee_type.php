<?php

class Employee_type extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('employee_type','employee_type_model');
        $this->lang->module_load('employee_type','employee_type');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'employee_type';
		$data['page'] = $this->config->item('template_admin') . "employee_type/index";
		$data['module'] = 'employee_type';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->employee_type_model->count();
		paging('employee_type_id');
		$this->_get_search_param();	
		$rows=$this->employee_type_model->getEmployeeTypes()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['type']!='')?$this->db->like('type',$params['search']['type']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->employee_type_model->getEmployeeTypes()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->employee_type_model->delete('EMPLOYEE_TYPES',array('employee_type_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('employee_type_id'))
        {
            $success=$this->employee_type_model->insert('EMPLOYEE_TYPES',$data);
        }
        else
        {
            $success=$this->employee_type_model->update('EMPLOYEE_TYPES',$data,array('employee_type_id'=>$data['employee_type_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['employee_type_id'] = $this->input->post('employee_type_id');
$data['type'] = $this->input->post('type');

        return $data;
   }
   
   	
	    
}