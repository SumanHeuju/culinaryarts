<?php

class Student_attendance extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('student_attendance','student_attendance_model');
        $this->lang->module_load('student_attendance','student_attendance');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'student_attendance';
		$data['page'] = $this->config->item('template_admin') . "student_attendance/index";
		$data['module'] = 'student_attendance';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$this->student_attendance_model->joins=array('STUDENTS');
		$total=$this->student_attendance_model->count();
		paging('student_attendance_id');
		$this->_get_search_param();	
		$rows=$this->student_attendance_model->getStudentAttendances()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['student_id']!='')?$this->db->where('student_id',$params['search']['student_id']):'';
($params['search']['check_in']!='')?$this->db->like('check_in',$params['search']['check_in']):'';
($params['search']['check_out']!='')?$this->db->like('check_out',$params['search']['check_out']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->student_attendance_model->getStudentAttendances()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->student_attendance_model->delete('STUDENT_ATTENDANCES',array('student_attendance_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('student_attendance_id'))
        {
            $success=$this->student_attendance_model->insert('STUDENT_ATTENDANCES',$data);
        }
        else
        {
            $success=$this->student_attendance_model->update('STUDENT_ATTENDANCES',$data,array('student_attendance_id'=>$data['student_attendance_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['student_attendance_id'] = $this->input->post('student_attendance_id');
$data['student_id'] = $this->input->post('student_id');
$data['date'] = $this->input->post('date');
$data['check_in'] = $this->input->post('check_in');
$data['check_out'] = $this->input->post('check_out');

        return $data;
   }
   
   	
	    
}