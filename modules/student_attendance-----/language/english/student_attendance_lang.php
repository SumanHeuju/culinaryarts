<?php


$lang['student_attendance_id'] = 'Id';
$lang['student_id'] = 'Student';
$lang['date'] = 'Date';
$lang['check_in'] = 'Check In';
$lang['check_out'] = 'Check Out';

$lang['create_student_attendance']='Create Student Attendance';
$lang['edit_student_attendance']='Edit Student Attendance';
$lang['delete_student_attendance']='Delete Student Attendance';
$lang['student_attendance_search']='Student Attendance Search';

$lang['student_attendance']='Student Attendance';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

