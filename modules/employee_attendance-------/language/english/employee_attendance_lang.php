<?php


$lang['employee_attendance_id'] = 'Employee Attendance Id';
$lang['employee_id'] = 'Employee Id';
$lang['date'] = 'Date';
$lang['check_in'] = 'Check In';
$lang['check_out'] = 'Check Out';
$lang['is_sync'] = 'Is Sync';
$lang['is_message_sent'] = 'Is Message Sent';

$lang['create_employee_attendance']='Create Employee Attendance';
$lang['edit_employee_attendance']='Edit Employee Attendance';
$lang['delete_employee_attendance']='Delete Employee Attendance';
$lang['employee_attendance_search']='Employee Attendance Search';

$lang['employee_attendance']='Employee Attendance';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

