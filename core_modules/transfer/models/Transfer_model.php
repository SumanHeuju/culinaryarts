<?php
class Transfer_model extends MY_Model
{
	var $joins=array();
    var $db2;
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('STUDENTS'=>$this->prefix.'students',
                             'STUDENT_ATTENDENCES'=>$this->prefix.'student_attendances');
		$this->_JOINS=array('STUDENT_ATTENDENCES'=>array('join_type'=>'LEFT','join_field'=>'student_attendances.student_attendance_id=students.student_id',
                                           'select'=>'student_attendances.student_attendance_id','alias'=>'student_attendances'),
                           
                            );        
    }

    public function insertAttendence($data){

        echo $this->db->insert($this->_TABLES['STUDENT_ATTENDENCES'],$data);
    }

    public function updateAttendence($data,$where){

        echo $this->db->update($this->_TABLES['STUDENT_ATTENDENCES'],$data,$where);
    }    
    
    public function getOdbcAttendence($where=null,$order_by=NULL){
        
        $this->db2->from('CHECKINOUT');
        (!is_null($where))?$this->db2->where($where,FALSE):null;
        
         (!is_null($order_by))?$this->db2->order_by($order_by):null;
        return $this->db2->get();
        
    }   

}