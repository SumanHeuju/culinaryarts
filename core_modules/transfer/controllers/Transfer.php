<?php
class Transfer extends Public_Controller{

	private $db2;
	public function __construct(){
		parent::__construct();
		$this->db2=$this->load->database('biomatrix',TRUE);
		$this->load->module_model('student','student_model');
		$this->load->module_model('student','student_attendance_model');
		$this->load->module_model('transfer','transfer_model');
		$this->transfer_model->db2=$this->db2;

	}


	public function import_attentence_data(){
		
		
		$where=array("CHECKTIME >=#". date('Y-m-d')."#"=>null);
		//$where =null;
		//echo "<pre>";
		$attendances=$this->transfer_model->getOdbcAttendence($where);

		

	
		foreach($attendances->result_array() as $row){


			$date=strtotime($row['CHECKTIME']);

			$student=$this->student_model->getStudents(array('bio_user_id'=>$row['USERID']))->row_array();
			
			
			
			
			if(!empty($student)){

				$attendance=$this->student_attendance_model->getStudentAttendances(array('student_id'=>$student['student_id'],'date'=>date('Y-m-d')))->row_array();


				if(empty($attendance)){
					$data=array('student_id'=>$student['student_id'],'date'=>date('Y-m-d',$date),'check_in'=>date('H:i:s',$date),'is_sync'=>1);
					$this->transfer_model->insertAttendence($data);
				}
				else if(is_null($attendance['check_out']) && strtotime($attendance['date']." ".$attendance['check_in']) < $date ){
					$this->transfer_model->updateAttendence(array('check_out'=>date('H:i:s',$date)),array('student_attendance_id'=>$attendance['student_attendance_id']));
				}
			}
			


		}
	}

	public function send_notification(){
		$this->load->library('texter');

		$this->load->module_model('student','student_attendance_model');

		$this->student_attendance_model->joins=array('STUDENTS');
		$students=$this->student_attendance_model->getStudentAttendances(array('is_message_sent !='=>2,'date'=>date('Y-m-d')))->result_array();

		
		foreach($students as $s){
			if($s['guardian_contact_no']!=""){
				$status=(is_null($s['check_out']))?' checked in ':' checked out';
				echo "sending sms to " . $s['guardian_contact_no'] ."<br/>";
		$message="Dear " . $s['guardian_name'] . ", " . $s['first_name'] . " has " . $status;

			$confirm=$this->texter->sendSMS($s['guardian_contact_no'],$message);
			if($confirm!=""){
				$where=array('student_attendance_id'=>$s['student_attendance_id']);
				if(is_null($s['check_out'])){
					$updatedata=array('is_message_sent'=>'1');
				}
				else{
					$updatedata=array('is_message_sent'=>'2');
				}
				$this->student_attendance_model->update('STUDENT_ATTENDANCES',$updatedata,$where);
				echo "message sent";
			}
		}

		}

	}


}