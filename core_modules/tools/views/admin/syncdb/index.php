<div region="center" border="false">
<div style="padding:20px">
<h2>Sync Database</h2>
<form id="sql-form" method="post">

<button type="submit" id="syncdb-btn">Sync Database</button>

</form>
<div id="result"></div>
</div>
</div>

<script>

	$(document).on("ready",function(){
		$("#syncdb-btn").on("click",function(){
			$(this).attr("disabled",true);
			var $result=$("#result").html("Syncronizing Data Please wait ........");
			$.post('<?php echo site_url('tools/admin/syncdb/sync')?>',function(){
				$result.html("Sync Successful");
			});
		});
	});
</script>